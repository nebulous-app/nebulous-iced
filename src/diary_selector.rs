use std::{path::Path, sync::Arc};

use electra::{BufferingStorageAccess, DiskStorageAccess, Manager as FileManager};
use error_stack::{Result, ResultExt};
use error_stack_error_macro::error;
use iced::{
    alignment::Horizontal,
    border::Radius,
    widget::{column, *},
    Alignment, Border, Command, Length,
};
use lib_nebulous::{Config, DiaryState};

use crate::{
    diary_creator::DiaryCreatorState,
    settings::{self, Settings},
    Message, Nebulous, NebulousState, ShowOnScreenError,
};

error!(pub InitializeError; "Failed to intitialize Nebulous");

pub async fn intitialize_nebulous(
    data_dir: &'static Path,
) -> Result<DiarySelectorState, InitializeError> {
    let config_storage_access =
        Arc::new(DiskStorageAccess::new(data_dir.to_owned()).change_context(InitializeError)?);

    let config = match FileManager::<Config<settings::Settings>, _>::new(
        Arc::clone(&config_storage_access),
        &(),
        (),
        (),
    )
    .await
    .change_context(InitializeError)?
    {
        Some(config) => config,
        None => {
            let config = Config::new(Settings::default());

            FileManager::new_from_data(Arc::clone(&config_storage_access), &(), config, (), ())
                .await
                .change_context(InitializeError)?
        }
    };

    #[cfg(all(feature = "auto-updates", not(debug_assertions)))]
    {
        // TODO: Figure out auto updates
    }

    Ok(DiarySelectorState {
        config,
        selected: None,
    })
}

#[derive(Debug, Clone)]
pub enum DiarySelectorMessage {
    CreateNew,
}

impl From<DiarySelectorMessage> for Message {
    fn from(msg: DiarySelectorMessage) -> Self {
        Message::DiarySelector(msg)
    }
}

pub struct DiarySelectorState {
    config: FileManager<Config<Settings>, DiskStorageAccess>,
    selected: Option<Selection>,
}

struct Selection {
    idx: usize,
    state: DiaryState<BufferingStorageAccess<DiskStorageAccess>>,
}

impl core::fmt::Debug for DiarySelectorState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("StartPageState")
    }
}

pub fn view(state: &DiarySelectorState) -> impl Into<iced::Element<'_, Message>> {
    container(
        column!(
            Text::new("Diaries"),
            Column::with_children(state.config.diaries().iter().map(|v| {
                container(Text::new(&v.0))
                    .style(|theme: &Theme, status| container::Appearance {
                        border: Border {
                            color: theme.palette().primary,
                            width: 2.,
                            radius: Radius::from(4.),
                        },
                        ..<Theme as container::DefaultStyle>::default_style(theme, status)
                    })
                    .into()
            })),
            row!(
                Button::new(Text::new("+")).on_press(DiarySelectorMessage::CreateNew.into()),
                Button::new(Text::new("Open"))
            )
            .spacing(8),
        )
        .spacing(8)
        .align_items(Alignment::Center),
    )
    .width(Length::Fill)
    .align_x(Horizontal::Center)
    .padding([8, 8])
}

pub fn update(
    state: &mut Nebulous,
    message: DiarySelectorMessage,
) -> Result<Command<Message>, ShowOnScreenError> {
    match message {
        DiarySelectorMessage::CreateNew => {
            state.goto(NebulousState::DiaryCreator(DiaryCreatorState::default()))
        }
    }

    Ok(Command::none())
}
