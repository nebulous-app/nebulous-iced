use std::convert::Infallible;

use error_stack::Result;
use lib_nebulous::TryRepair;
use log::warn;
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Language {
    #[serde(rename = "en")]
    En,
}

impl Language {
    // TODO: Figure out documentation
    // pub fn docs(&self) -> WindowUrl {
    //     match self {
    //         Language::En => WindowUrl::App("docs/en/index.html".into()),
    //     }
    // }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Settings {
    pub language: Language,
    pub category_prediction: bool,
    pub check_for_updates: Option<bool>,
}

impl Default for Settings {
    fn default() -> Self {
        Self {
            language: Language::En,
            category_prediction: true,
            check_for_updates: cfg!(feature = "auto-updates").then_some(true),
        }
    }
}

impl TryRepair for Settings {
    type Error = Infallible;

    fn try_repair(&mut self) -> Result<(), Infallible> {
        match (cfg!(feature = "auto-updates"), self.check_for_updates) {
            (true, Some(_)) | (false, None) => {}
            (false, Some(_)) => {
                self.check_for_updates = {
                    warn!("Check for updates was `Some` in a build without auto updates");
                    None
                }
            }
            (true, None) => {
                self.check_for_updates = {
                    warn!("Check for updates was `None` in a build with auto updates");
                    Some(false)
                }
            }
        }

        Ok(())
    }
}
