use std::{
    collections::VecDeque,
    path::{Path, PathBuf},
    sync::Arc,
};

use diary_creator::{DiaryCreatorMessage, DiaryCreatorState};
use diary_selector::{
    intitialize_nebulous, DiarySelectorMessage, DiarySelectorState, InitializeError,
};
use error_stack::{Context, Report, Result};
use error_stack_error_macro::error;
use futures::channel::mpsc::{self, UnboundedSender};
use iced::{
    widget::column, widget::*, window, Application, Command, Settings as IcedSettings, Size,
};

mod diary_creator;
mod diary_selector;
mod settings;

fn main() -> iced::Result {
    Nebulous::run(IcedSettings {
        window: window::Settings {
            size: Size::new(500.0, 800.0),
            ..window::Settings::default()
        },
        ..IcedSettings::default()
    })
}

error!(pub ShowOnScreenError; "Showing error to screen");

struct Nebulous {
    error: UnboundedSender<Report<ShowOnScreenError>>,
    errors: VecDeque<Arc<Report<ShowOnScreenError>>>,
    pub data_dir: &'static Path,
    stack: Vec<NebulousState>,
}

impl Nebulous {
    pub fn show_error<T: Context>(&self, err: Report<T>) {
        self.error
            .unbounded_send(err.change_context(ShowOnScreenError))
            .expect("Sending the error to have succeeded");
    }

    pub fn show_error_closure(&self) -> impl Fn(Report<ShowOnScreenError>) + Clone {
        let err_tx = self.error.clone();

        move |err| {
            err_tx
                .unbounded_send(err)
                .expect("Sending the error to have succeeded");
        }
    }

    pub fn state(&self) -> &NebulousState {
        self.stack
            .last()
            .expect("there to be something on the stack")
    }

    pub fn state_mut(&mut self) -> &mut NebulousState {
        self.stack
            .last_mut()
            .expect("there to be something on the stack")
    }

    pub fn goto(&mut self, state: NebulousState) {
        if matches!(self.stack.last(), Some(NebulousState::Loading)) {
            self.stack.pop();
        }

        self.stack.push(state)
    }

    pub fn back(&mut self) {
        if self.stack.len() > 1 {
            self.stack.pop();
        }
    }
}

enum NebulousState {
    Loading,
    DiarySelector(DiarySelectorState),
    DiaryCreator(DiaryCreatorState),
}

error!(pub ShouldNotHaveBeenCloned; "The message should not have been cloned");
error!(pub UnexpectedMessage; "The diary is in the wrong state for the given message");

#[macro_export]
macro_rules! try_unwrap {
    ($state: expr => $v: ident) => {
        match $state {
            crate::NebulousState::$v(state) => Ok(state),
            _ => Err(error_stack::Report::new(crate::UnexpectedMessage)
                .change_context(crate::ShowOnScreenError)),
        }
    };
}

#[derive(Debug, Clone)]
enum Message {
    Nop,
    CloseError,
    Back,
    NewError(Arc<Report<ShowOnScreenError>>),
    Intitialized(Arc<Result<DiarySelectorState, InitializeError>>),
    DiarySelector(DiarySelectorMessage),
    DiaryCreator(DiaryCreatorMessage),
}

impl Application for Nebulous {
    type Executor = iced::executor::Default;
    type Message = Message;
    type Theme = iced::Theme;
    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, iced::Command<Self::Message>) {
        let (tx, rx) = mpsc::unbounded();

        let data_dir = Box::leak::<'static>(
            match dirs::data_dir().ok_or_else(|| {
                Report::new(InitializeError).attach_printable("Failed to get data directory")
            }) {
                Ok(v) => v,
                Err(e) => {
                    tx.unbounded_send(e.change_context(ShowOnScreenError))
                        .expect("Sending the error to work");
                    PathBuf::new()
                }
            }
            .join("nebulous")
            .into_boxed_path(),
        );

        (
            Nebulous {
                error: tx,
                errors: VecDeque::new(),
                data_dir,
                stack: vec![NebulousState::Loading],
            },
            Command::batch([
                Command::perform(intitialize_nebulous(data_dir), |v| {
                    Message::Intitialized(Arc::new(v))
                }),
                Command::run(rx, |v| Message::NewError(Arc::new(v))),
            ]),
        )
    }

    fn title(&self) -> String {
        "Nebulous".to_owned()
    }

    fn update(&mut self, message: Self::Message) -> iced::Command<Self::Message> {
        match message {
            Message::Nop => {}
            Message::CloseError => {
                self.errors.pop_front();
            }
            Message::Back => self.back(),
            Message::NewError(e) => self.errors.push_back(e),
            Message::Intitialized(maybe_config) => match Arc::try_unwrap(maybe_config) {
                Ok(Ok(config)) => self.goto(NebulousState::DiarySelector(config)),
                Ok(Err(e)) => self.show_error(e.change_context(ShowOnScreenError)),
                Err(_) => self.show_error(
                    Report::new(ShouldNotHaveBeenCloned)
                        .attach_printable("Intitialized config message was cloned"),
                ),
            },
            Message::DiarySelector(msg) => match diary_selector::update(self, msg) {
                Ok(cmd) => return cmd,
                Err(e) => self.show_error(e),
            },
            Message::DiaryCreator(msg) => match diary_creator::update(self, msg) {
                Ok(cmd) => return cmd,
                Err(e) => self.show_error(e),
            },
        }

        Command::none()
    }

    fn view(&self) -> iced::Element<'_, Self::Message, Self::Theme, iced::Renderer> {
        if let Some(error) = self.errors.front() {
            return column!(
                Text::new(format!("{}", error)),
                Button::new(Text::new("Close")).on_press(Message::CloseError),
            )
            .into();
        }

        match self.state() {
            NebulousState::Loading => Text::new("Loading...").into(),
            NebulousState::DiarySelector(state) => diary_selector::view(state).into(),
            NebulousState::DiaryCreator(state) => diary_creator::view(state).into(),
        }
    }

    fn theme(&self) -> Theme {
        Theme::KanagawaWave
    }
}
