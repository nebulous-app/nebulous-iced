use error_stack::{Report, Result};
use iced::{
    alignment::Horizontal,
    widget::{column, *},
    Alignment, Command, Length,
};
use rfd::AsyncFileDialog;
use std::path::PathBuf;
use DiaryCreatorMessage as M;

use crate::{try_unwrap, Message, Nebulous, ShowOnScreenError};

pub struct DiaryCreatorState {
    encryption: bool,
    password: String,
    confirm_password: String,
    path: Option<PathBuf>,
    folder_picker_error: Option<&'static str>,
    creation_error: Option<&'static str>,
}

impl Default for DiaryCreatorState {
    fn default() -> Self {
        DiaryCreatorState {
            encryption: true,
            password: String::new(),
            confirm_password: String::new(),
            path: None,
            folder_picker_error: None,
            creation_error: None,
        }
    }
}

#[derive(Debug, Clone)]
pub enum DiaryCreatorMessage {
    UpdateEncryption(bool),
    UpdatePassword(String),
    UpdateConfirmPassword(String),
    OpenFilePicker,
    UpdatePath(PathBuf, Option<&'static str>),
}

impl From<DiaryCreatorMessage> for Message {
    fn from(msg: DiaryCreatorMessage) -> Self {
        Message::DiaryCreator(msg)
    }
}

async fn pick_folder(
    prev_dir: Option<PathBuf>,
    show_error: impl Fn(Report<ShowOnScreenError>),
) -> Message {
    let mut builder = AsyncFileDialog::new().set_can_create_directories(true);

    builder = match prev_dir {
        Some(dir) => builder.set_directory(dir),
        None => match dirs::home_dir() {
            Some(dir) => builder.set_directory(dir),
            None => builder,
        },
    };

    let maybe_folder = builder.pick_folder().await;

    let folder = match maybe_folder {
        Some(folder) => folder.path().to_owned(),
        None => return Message::Nop,
    };

    if !folder.is_dir() {
        return M::UpdatePath(folder, Some("Path isn't a directory")).into();
    }

    let mut read = match folder.read_dir() {
        Ok(v) => v,
        Err(e) => {
            show_error(Report::from(e).change_context(ShowOnScreenError));
            return Message::Nop;
        }
    };

    if read.next().is_some() {
        return M::UpdatePath(
            folder,
            Some("The folder must be empty (Nebulous would trash it)"),
        )
        .into();
    }

    M::UpdatePath(folder, None).into()
}

pub fn update(
    nebulous: &mut Nebulous,
    msg: DiaryCreatorMessage,
) -> Result<Command<Message>, ShowOnScreenError> {
    let state = try_unwrap!(nebulous.state_mut() => DiaryCreator)?;

    match msg {
        M::UpdateEncryption(encryption) => state.encryption = encryption,
        M::UpdatePassword(password) => state.password = password,
        M::UpdateConfirmPassword(confirm_password) => state.confirm_password = confirm_password,
        M::OpenFilePicker => {
            return Ok(Command::perform(
                pick_folder(state.path.to_owned(), nebulous.show_error_closure()),
                |v| v,
            ));
        }
        M::UpdatePath(path, err) => {
            state.path = Some(path);
            state.folder_picker_error = err;
        }
    }

    Ok(Command::none())
}

fn error(state: &DiaryCreatorState) -> &'static str {
    if let Some(err) = state.folder_picker_error {
        return err;
    }

    if let Some(err) = state.creation_error {
        return err;
    }

    ""
}

pub fn view(state: &DiaryCreatorState) -> impl Into<iced::Element<'_, Message>> {
    container(
        column!(
            Text::new("Create a Diary"),
            Checkbox::new("Encryption", state.encryption)
                .on_toggle(|v| M::UpdateEncryption(v).into()),
            {
                let passwd = TextInput::new("Password", &state.password).secure(true);

                match state.encryption {
                    true => passwd.on_input(|v| M::UpdatePassword(v).into()),
                    false => passwd,
                }
            },
            {
                let confirm = TextInput::new("Confirm Password", &state.confirm_password).secure(true);

                match state.encryption {
                    true => confirm.on_input(|v| M::UpdateConfirmPassword(v).into()),
                    false => confirm,
                }
            },
            Text::new(match state.encryption {
                true => "WARNING: Your password is your encryption key and your diary will be unrecoverable if you lose it. Consider using a password manager.",
                false => "WARNING: Anyone with access to your computer can read your diary."
            }),
            row!(Button::new("Browse...").on_press(M::OpenFilePicker.into()), Text::new(match &state.path { 
                               None => "No folder selected".into(),
                Some(path) => path.to_string_lossy()
            })),
            row!(
                Button::new(Text::new("Create")),
                Button::new(Text::new("Cancel")).on_press(Message::Back)
            )
            .spacing(8),
            Text::new(error(state))
        )
        .spacing(8)
        .align_items(Alignment::Center),
    )
    .width(Length::Fill)
    .align_x(Horizontal::Center)
    .padding([8, 8])
}
